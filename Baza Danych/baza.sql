--
-- PostgreSQL database cluster dump
--

-- Started on 2018-01-25 20:33:26

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Roles
--

CREATE ROLE "Logowanie";
ALTER ROLE "Logowanie" WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'md58e8b93ca1ff65814b2bd4e6a807e9fee';
COMMENT ON ROLE "Logowanie" IS 'Dostep do Konta ';
CREATE ROLE "Rejestracja";
ALTER ROLE "Rejestracja" WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'md5ea60e7681bd25bc30961a99679ea1c38';
COMMENT ON ROLE "Rejestracja" IS 'Tworzenie kont i email';
CREATE ROLE postgres;
ALTER ROLE postgres WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD 'md5d944c36ad4793aa5d24a672ce36f1cd6';
CREATE ROLE rola;
ALTER ROLE rola WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN NOREPLICATION NOBYPASSRLS;
COMMENT ON ROLE rola IS 'kupa';


--
-- Role memberships
--

GRANT pg_read_all_settings TO rola GRANTED BY postgres;
GRANT pg_read_all_stats TO rola GRANTED BY postgres;




--
-- Database creation
--

REVOKE CONNECT,TEMPORARY ON DATABASE template1 FROM PUBLIC;
GRANT CONNECT ON DATABASE template1 TO PUBLIC;


\connect postgres

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.0

-- Started on 2018-01-25 20:33:26

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2153 (class 1262 OID 12292)
-- Dependencies: 2152
-- Name: postgres; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- TOC entry 1 (class 3079 OID 12278)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2155 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 196 (class 1259 OID 16453)
-- Name: Fotografowie; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "Fotografowie" (
    "id" SERIAL NOT NULL,
    "E-Mail" text NOT NULL,
    "Haslo" text NOT NULL,
    "Weryfikacja" boolean DEFAULT false NOT NULL,
    "Data(ważność)" date NOT NULL
);


ALTER TABLE "Fotografowie" OWNER TO postgres;

--
-- TOC entry 2156 (class 0 OID 0)
-- Dependencies: 196
-- Name: TABLE "Fotografowie"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "Fotografowie" IS 'nie komentuj bo po co.';


--
-- TOC entry 2157 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN "Fotografowie"."E-Mail"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "Fotografowie"."E-Mail" IS 'klucz główny';


--
-- TOC entry 2147 (class 0 OID 16453)
-- Dependencies: 196
-- Data for Name: Fotografowie; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "Fotografowie" ("E-Mail", "Haslo", "Weryfikacja", "Data(ważność)") FROM stdin;
Jasiek@wp.pl	szyfrowane	f	2018-02-25
mamawojtka@wp.pl	wojtek	f	2015-03-23
tatamamy@onet.pl	mama	t	2011-04-23
\.


--
-- TOC entry 2025 (class 2606 OID 16460)
-- Name: Fotografowie Użytkownicy_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Fotografowie"
    ADD CONSTRAINT "Użytkownicy_pkey" PRIMARY KEY ("id");


--
-- TOC entry 2158 (class 0 OID 0)
-- Dependencies: 196
-- Name: Fotografowie; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE "Fotografowie" TO "Logowanie";
GRANT INSERT ON TABLE "Fotografowie" TO "Rejestracja";
GRANT all on sequence "Fotografowie_id_seq" to "Rejestracja";

-- Completed on 2018-01-25 20:33:26

--
-- PostgreSQL database dump complete
--

\connect template1

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.0

-- Started on 2018-01-25 20:33:26

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2144 (class 1262 OID 1)
-- Dependencies: 2143
-- Name: template1; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE template1 IS 'default template for new databases';


--
-- TOC entry 1 (class 3079 OID 12278)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2146 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


-- Completed on 2018-01-25 20:33:26

--
-- PostgreSQL database dump complete
--

-- Completed on 2018-01-25 20:33:26

--
-- PostgreSQL database cluster dump complete
--

