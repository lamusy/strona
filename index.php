﻿<!DOCTYPE html>
<html lang="pl" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>bez nazwy</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="stylesheet" href="css/glowny.css" type="text/css">
    <link rel="stylesheet" href="css/fontello/css/fontello.css" type="text/css">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous" />
</head>
<body style="background-color:#fffffa;">
<?php
 session_start();
            if(isset($_COOKIE["weryfikacja"]))
             {
                 if($_COOKIE["weryfikacja"]=="true")
                {
                    echo '
                    <center>
                    <div class="alert alert-success  fade show" role="alert" style="position: flex;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong>Gratulacje!</strong> weryfikacja zakończona pomyślnie !
                        </div>
                        </center>
                        ';
                        setcookie("weryfikacja", "a", time() - 3600);
                }else if ($_COOKIE["weryfikacja"]=="false")
                {
                    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert" style="width:100%;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Uwaga!</strong> weryfikacja zakończona niepowodzeniem !
                     </div>';
                    setcookie("weryfikacja", "a", time() - 3600);
                }
            }
            if(isset($_COOKIE["sesja"]))
             {
                 if ($_COOKIE["sesja"]=="false")
                {
                    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert" style="width:100%;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Uwaga!</strong> Sesja wygasła zaloguj się ponownie !
                     </div>';
                    setcookie("sesja", "a", time() - 3600);
                }
            }
            if(isset($_SESSION['id']))
	        {
                header("Location:panel.php");
            }
            ?>
    <div class="container-fluid" >
        <!--gorny pasek-->
		<div class="row justify-content-between" id="gornypasek">
            <!--logo-->
			<div class="col-sm-3" >
			    <img src="data/ikona.png" class="img-fluid" alt="Logo" id="logo">
            </div>
            <!--alert-->
			 <!--logowanie-->
			<div class="col-sm-3">
			    <div class="row">
				    <div  class="col-6 rejestracja" data-toggle="modal" data-target="#logowanie">
				        zaloguj
				    </div>
				     <div class="col-6 rejestracja" data-toggle="modal" data-target="#rejestracja">
				        zarejestruj
				    </div>
				</div>
			</div>
        </div>
         <!--srodek-->
		<div class="row" >
         <!--zdjęcia -->
             <div  class="col-lg-12">
             <div class="row">
                <div id="myCarousel" class="carousel slide bg-inverse w-100 ml-auto mr-auto" data-ride="carousel">
                     <ol class="carousel-indicators">
                          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                          <li data-target="#myCarousel" data-slide-to="1"></li>
                          <li data-target="#myCarousel" data-slide-to="2"></li>
                     </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                                <img src="data/glowna_1.jpg" class="img-fluid" alt="Logo">
                        </div>
                         <div class="carousel-item">
                                <img src="data/glowna_2.jpg" class="img-fluid" alt="Logo">
                         </div>
                        <div class="carousel-item">
                                <img src="data/glowna_3.jpg" class="img-fluid" alt="Logo">
                        </div>
                    </div>
                     <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                     <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                         <span class="carousel-control-next-icon" aria-hidden="true"></span>
                         <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            </div>
        </div>
         <!--stopka -->
		<div class="row  justify-content-around " style="text-align:center">
                <div  class="col"  style="border-top:4px solid#e7e7d9;">
                     <h3>kontakt</h3>
                     <span aria-hidden="true">&times;</span>
                     Masz jakieś pytania propozycje pisz Śmiało  na e-mail@cos.pl </br>
                     nie zapomnij odwiedzic nas na portalach społecznościowych </br>
                </div>
                 <div  class="col-sm" style="border-top:4px solid#e7e7d9;">
                    <h4>zapoznaj sie z naszym regulaminem</h4>
                </div>
        </div>
        <div  class="row" style="text-align:center;margin-top:5px;">
            <div  class="col" id="ale">
                   Copyright © 2018 My All Rights reserved.
            </div>
        </div>
</div>




<div class="modal fade" id="logowanie" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="logowanie-alert">
             <div class="modal-header">
                 <h2>Logowanie</h2>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                 </button>
            </div>
            <div class="modal-body">
            </br>
                <form action="">
                <fieldset class="form-group">
                    <label for="first_name">Login</label>
                    <span  class="blad" id="l-mail">zly adres e-mail </span>
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="icon-user"></i></span>
                        <input type="text" class="form-control" aria-label="Amount (rounded to the nearest dollar)" id="loginmail" placeholder="e-mail lub identyfikator" onkeyup="sprmail(this)" onBlur="zmiana_logowanie()">
                    </div>
                </fieldset>
                <fieldset class="form-group">
                    <label for="last_name">Hasło</label>
                    <span  class="blad" id="b_log_haslo">nieprawidłowe hasło</span>
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="icon-lock"></i></span>
                        <input type="password" class="form-control" aria-label="Amount (rounded to the nearest dollar)" placeholder="hasło" id="log_haslo" onkeyup="logowanie_haslo()">
                    </div>
                </fieldset>
                <button type="button" class="btn btn-default"  onclick="logowanie()">Zaloguj</button>
                <span  id="logowanie_proces" style="color:green;"></span>
                </form>
              </div>
        </div>
    </div>
 </div>

 <div class="modal fade" id="rejestracja" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" id="rejestracja-alert">
                 <div class="modal-header">
                     <h2>Rejestracja</h2>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                     </button>
                </div>
                <div class="modal-body">
                </br>
                    <form action="" method="POST">
                    <fieldset class="form-group">
                        <label for="first_name">e-mail</label>
                        <span  class="blad" id="b-mail">zly adres e-mail </span>
                        <div class="input-group">
                            <span class="input-group-addon"> <i class="icon-user"></i></span>
                            <input type="text" class="form-control" aria-label="Amount (rounded to the nearest dollar)" placeholder="e-mail" id="mail" onkeyup="sprmail(this)" onBlur="zmiana()">
                        </div>
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="last_name">Hasło</label>
                        <span  class="blad" id="bhaslo1">hasło musi mieć minimum 6 znaków maksymalnie 30 i nie moze zawierać "'?{/}[;]()</span>
                        <div class="input-group">
                        <span class="input-group-addon"> <i class="icon-lock"></i></span>
                        <input type="password" class="form-control" aria-label="Amount (rounded to the nearest dollar)" placeholder="hasło" id="haslo1" onkeyup="sprhaslo1()">
                        </div>
                    </fieldset>
                    <fieldset class="form-group">
                            <label for="last_name">potwierdz Hasło</label>
                            <span  class="blad" id="bhaslo2">hasła różnią się od siebie </span>
                            <div class="input-group">
                            <span class="input-group-addon"> <i class="icon-lock"></i></span>
                            <input type="password" class="form-control" aria-label="Amount (rounded to the nearest dollar)" placeholder="powtórz hasło" id="haslo2" onkeyup="sprhaslo2()">
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                        <label for="last_name">Regulamin</label><br/>
                        <span  class="blad" id="bregulamin" style="color:red;display:none;">musisz zaakceptować regulamin </span>
                            <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="regulamin" onclick="checkbox()">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Akceptuje regulamin</span>
                        </label>
                        </fieldset>
                    <fieldset class="form-group">
                            <label for="last_name">antybot</label>
                            <span  class="blad" id="badbot">botów nie przyjmujemy</span>
                            <div class="input-group">
                                <div class="g-recaptcha"data-sitekey="6LfAoT8UAAAAAFDADoj-tJ6YVWbhouufj4ebKNqy" ></div>
                            </div>
                    </fieldset>
                    <button type="button" class="btn btn-default" onclick="zarejestruj()">Zarejestruj</button>
                    <span  id="rejestracja_proces" style="color:green;"></span>
                    </form>
                  </div>
            </div>
        </div>
     </div>
















    <script src='js/glowna.js'></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>
</html>