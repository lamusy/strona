function sprawdzciag(tekst)
{
var zakaz="\"\'?{/}[;]()";
for (var i =1 ;i<tekst.length;i++)
    {
    for(var j= 0;j<zakaz.length;j++)
        {
            if(tekst[i]==zakaz[j])
            {
                return false;
            }
        }
    }
    return true;
}

function klient_imie()
{
    var imie = document.querySelector('#dodawanie_klienta_input_imie').value ;
    if  (!sprawdzciag(imie) || imie.length>60||imie.length<1)
    {
         document.querySelector('#dodawanie_klienta_input_imie').style.borderBottomColor="red";
         document.querySelector('#dodawanie_klienta_input_imie').style.color="red";
        return false ;
    }else
    {
        document.querySelector('#dodawanie_klienta_input_imie').style.borderBottomColor="black";
         document.querySelector('#dodawanie_klienta_input_imie').style.color="black";
        return true;
    }
   
}
function klient_nazwisko()
{
    var nazwisko = document.querySelector('#dodawanie_klienta_input_nazwisko').value ;
    if  (!sprawdzciag(nazwisko) || nazwisko.length>60||nazwisko.length<1)
    {
         document.querySelector('#dodawanie_klienta_input_nazwisko').style.borderBottomColor="red";
         document.querySelector('#dodawanie_klienta_input_nazwisko').style.color="red";
        return false ;
    }else
    {
        document.querySelector('#dodawanie_klienta_input_nazwisko').style.borderBottomColor="black";
         document.querySelector('#dodawanie_klienta_input_nazwisko').style.color="black";
        return true;
    }
   
}
function klient_email()
{
    var email = document.querySelector('#dodawanie_klienta_input_email').value ;
    if  (!sprawdzciag(email) || email.length>60 ||email.length<1)
    {
         document.querySelector('#dodawanie_klienta_input_email').style.borderBottomColor="red";
         document.querySelector('#dodawanie_klienta_input_email').style.color="red";
        return false ;
    }else
    {
        document.querySelector('#dodawanie_klienta_input_email').style.borderBottomColor="black";
         document.querySelector('#dodawanie_klienta_input_email').style.color="black";
        return true;
    }
}
function klient_telefon()
{
    var tel = document.querySelector('#dodawanie_klienta_input_tel').value ;
    if  (!sprawdzciag(tel) || tel.length>14)
    {
         document.querySelector('#dodawanie_klienta_input_tel').style.borderBottomColor="red";
         document.querySelector('#dodawanie_klienta_input_tel').style.color="red";
        return false ;
    }else
    {
        document.querySelector('#dodawanie_klienta_input_tel').style.borderBottomColor="black";
         document.querySelector('#dodawanie_klienta_input_tel').style.color="black";
        return true;
    }
}
function klient_zdjecie()
{
    if (document.querySelector('#klient_zdjecie_input').files.length > 0) 
    {
    var  nazwa=document.querySelector('#klient_zdjecie_input').files[0].name
    document.querySelector('#klient_zdjecie_input_nazwa').innerHTML=   nazwa.substr(nazwa.length-20, nazwa.length);
    return true;
    }

}
function klient_dodawanie()
{
    if(klient_imie()&&klient_nazwisko()&&klient_email()&&klient_telefon()&&klient_telefon())
    {
        document.querySelector('#dodawanie_klienta_info').innerHTML ='<span style="color:green">Trwa dodawanie Proszę czekać</span>';
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function()
        {
            if (this.readyState == 4 && this.status == 200)
            {
                if(isNaN(this.responseText))
                {    
                    
                       
                       document.querySelector('#dodawanie_klienta_info').innerHTML = this.responseText;
                   
               
                }else
                {
                    if(document.querySelector('#klient_zdjecie_input').files.length===0)
                    {
                        document.querySelector('#dodawanie_klienta_info').innerHTML ="<span style='color:green'>klient dodany pomyślnie</span>"
                    }else 
                    {
                       
                        klient_dodawanie_zdj()
                    }
                   
                }
        
            }
        }
        xmlhttp.open("POST", "php/skrypty/dodawanie_klienta.php", false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("imie="+ document.querySelector('#dodawanie_klienta_input_imie').value+"&telefon="+ document.querySelector('#dodawanie_klienta_input_tel').value+"&nazwisko="+ document.querySelector('#dodawanie_klienta_input_nazwisko').value+"&email="+ document.querySelector('#dodawanie_klienta_input_email').value)
    }else
    {
        document.querySelector('#dodawanie_klienta_info').innerHTML ="<span style='color:red'>błędnie wypełniony formularz</span>";
    }
} 
  
  function klient_dodawanie_zdj()
  {

    var data = new FormData();    
    data.append("plik", document.querySelector('#klient_zdjecie_input').files[0]);

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            if(isNaN(this.responseText))
            {
                document.querySelector('#dodawanie_klienta_info').innerHTML ="<span style='color:green'>klient dodany pomyślnie</span>"
            }else
            { 
                document.querySelector('#dodawanie_klienta_info').innerHTML = this.responseText;
            }
        }
    }

    xmlhttp.open("POST", "php/skrypty/dodawanie_klienta_zdj.php", true);
    xmlhttp.send(data);
  }
  
  
  
  
  
  
  
  
  
  
  
    /*  
















function wyslijPlik() {
	var plik=document.getElementById("plik").files[0];
	
	var formularz=new FormData(); //tworzymy nowy formularz do wysłania
	formularz.append("plik", plik); //dodajemy do formularza pole z naszym plikiem
	
    /* wysyłamy formularz za pomocą AJAX */
    /*
	var xhr=new XMLHttpRequest();
	xhr.upload.addEventListener("progress", postepWysylania, false);
	xhr.addEventListener("load", zakonczenieWysylania, false);
	xhr.addEventListener("error", bladWysylania, false);
	xhr.addEventListener("abort", przerwanieWysylania, false);
	xhr.open("POST", "php/skrypty/dodawanie_klienta.php", true);
	xhr.send(formularz);
}

function postepWysylania(event) {
	var procent=Math.round((event.loaded/event.total)*100);
	document.getElementById("status").innerHTML="Wysłano "+konwersjaBajtow(event.loaded)+" z "+konwersjaBajtow(event.total)+" ("+procent+"%)";
	document.getElementById("postep").value=procent;
}

function zakonczenieWysylania(event) {
    document.getElementById("status").innerHTML=event.target.responseText;
    console.log("to tu bylo ");
    console.log(event.target.responseText);
}

function bladWysylania(event) {
	document.getElementById("status").innerHTML="Wysyłanie nie powiodło się";
}

function przerwanieWysylania(event) {
	document.getElementById("status").innerHTML="Wysyłanie zostało przerwane";
}

function konwersjaBajtow(bajty) {
	var kilobajt=1024;
	var megabajt=kilobajt*1024;
	var gigabajt=megabajt*1024;
	var terabajt=gigabajt*1024;
	
	if (bajty>=0 && bajty<kilobajt) return bajty+" B";
	else if(bajty>=kilobajt && bajty<megabajt) return Math.round(bajty/kilobajt)+" kB";
	else if(bajty>=megabajt && bajty<gigabajt) return Math.round(bajty/megabajt)+" MB";
	else if(bajty>=gigabajt && bajty<terabajt) return Math.round(bajty/gigabajt)+" GB";
	else if(bajty>=terabajt) return Math.round(bajty/terabajt)+" TB";
	else return bajty+" B";
}


    
*/