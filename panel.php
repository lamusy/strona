﻿<!DOCTYPE html>
<?php
session_start();
if(!isset($_SESSION['login']))
	{
    setcookie("sesja", "false", time() + 3600);
    header("Location:index.php");
  }
?>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>panel </title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="css/panel.css" type="text/css">
    <link rel="stylesheet" href="css/fontello/css/fontello.css" type="text/css">
    <link rel="stylesheet" href="css/fontello/css/camera.css" type="text/css">
    <link rel="stylesheet" href="css/fontello/css/animation.css" type="text/css">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous" />
</head>
<body style="background-color:#f3f3ef;">
    <div class="container-fluid">
      <!--panel-->
      <div class="row " >
        <!--logo-->
        <div class="col-sm-3 col-lg-2 przybornik " style="text-align:center;">
          <img src="data/ikona.png" alt="Logo" id="logo">
        </div>
         <!--opcje-->
        <div class=" col-sm order-lg-12 przybornik">
          <div class="row justify-content-end" >
            <!--aktywne konto-->
              <div class="col-auto">
                <?php
            
                  $baza = pg_connect("host=localhost dbname=postgres user=Pracownik password=5w1n4");
                  $query = "SELECT \"Data(ważność)\" FROM \"Fotografowie\" WHERE \"E-Mail\" ='".$_SESSION['login']."';" ;
                  $result = pg_query($query) ;
                  if(pg_num_rows($result)>0)
                  {
                    $wynik = pg_fetch_row($result);
                    $waznosc=round( (mktime (0,0,0,(int)$wynik[0][5]*10 +(int)$wynik[0][6],(int)$wynik[0][8]*10 +(int)$wynik[0][9],(int)$wynik[0]) - mktime (0,0,0,date("m"),date("d"),date("Y")))/86400 );
                    echo '
                      <div  class="komunikat" id="komunikat_sklep" style="display:';
                    if($waznosc <=0)
                    {
                      $_SESSION["waznosc"] = false;
                      echo '
                        block" >
                        <div class="row justify-content-center" >
                          <div class=" col"style="text-align: center;">
                            <h1 style="color:Red;">twoja usługa wygasła  </h1>
                            <h4>przyspiesz swoją pracę i wykup naszą usługe na kolejne miesiące
                          </div>
                        </div>
                        ';
                    }else if($waznosc <3)
                    {
                      $_SESSION["waznosc"] = true;
                    echo '
                    block" >
                      <div class="row justify-content-between" >
                        <div class=" col-11"style="text-align: center;">
                          <h1 style="color:Red;">twoja usługa niedługo wygaśnie,pozostały ci jeszcze: '.$waznosc.' dni</h1>
                          <h4>przyspiesz swoją pracę i wykup naszą usługe na kolejne miesiące
                        </div>
                        <div class=" col-sm-1">
                        <button type="button"  class="zamykanie_przycisk" aria-label="Close">
                        <span aria-hidden="true"  onclick="zmknij_sklep()">&times;</span>
                        </button>
                        </div>
                      </div>
                      ';
                    }else
                    {
                      $_SESSION["waznosc"] = true;
                    echo '
                    none" >
                      <div class="row justify-content-between" >
                        <div class=" col-11"style="text-align: center;">
                          <h1>twoja usługa jest aktywna,pozostały ci jeszcze: '.$waznosc.' dni</h1>
                          <h4>przyspiesz swoją pracę i wykup naszą usługe na kolejne miesiące
                        </div>
                        <div class=" col-sm-1">
                        <button type="button"  class="zamykanie_przycisk" aria-label="Close">
                        <span aria-hidden="true"  onclick="zmknij_sklep()">&times;</span>
                        </button>
                        </div>
                      </div>
                      ';
                    }
                    echo'  <div class="row justify-content-center" >
                        <div class=" col-10" style="color: Black;text-align: center;">
			                    <table class="table-striped table-responsive" >
			                      <thead>
			                        <tr>
			                          <th style="width:180px;">Cena</th>
			                          <th style="width:180px;">Czas</th>
                                <th>Kod promocyjny</th>
                                <th></th>
			                        </tr>
			                      </thead>
                            <tbody>
                              <form action="./php/weryfikacja_platnosci.php" method="POST">
			                          <tr>
			                            <td id="koszt1">50 zł </td>
			                            <td>1 Miesiąc </td>
                                  <td>
                                  <span  class="blad" id="zlykod1" >bledny kod</span>
                                  <input type="hidden"  name="cena" value="50" id="cena1">
                                  <input type="text" class="form-control"  id="promocja1" placeholder="wpisz kod promocyjny" name="kod" onBlur="sklep_kod(1)"></td>
                                  <td><button type="submit" class="btn btn-default" " id="zaplata1">Zapłać</button></td>
                                </tr>
                              </form>

                              <form action="./php/weryfikacja_platnosci.php" method="POST">
			                          <tr>
			                            <td id="koszt2">140 zł </td>
			                            <td>3 Miesiące </td>
                                  <td>
                                  <span  class="blad" id="zlykod2" >bledny kod</span>
                                  <input type="hidden"  name="cena" value="140" id="cena2">
                                  <input type="text" class="form-control"  id="promocja2" placeholder="wpisz kod promocyjny" name="kod" onBlur="sklep_kod(2)"></td>
                                  <td><button type="submit" class="btn btn-default" " id="zaplata2">Zapłać</button></td>
                                </tr>
                              </form>

                              <form action="./php/weryfikacja_platnosci.php" method="POST">
                              <tr>
                                <td id="koszt3">250 zł </td>
                                <td>6 Miesiący </td>
                                <td>
                                <span  class="blad" id="zlykod3" >bledny kod</span>
                                <input type="hidden"  name="cena" value="250" id="cena3">
                                <input type="text" class="form-control"  id="promocja3" placeholder="wpisz kod promocyjny" name="kod" onBlur="sklep_kod(3)"></td>
                                <td><button type="submit" class="btn btn-default" " id="zaplata3">Zapłać</button></td>
                              </tr>
                            </form>

                            <form action="./php/weryfikacja_platnosci.php" method="POST">
                            <tr>
                              <td id="koszt4">450 zł </td>
                              <td>12 Miesiący </td>
                              <td>
                              <span  class="blad" id="zlykod4" >bledny kod</span>
                              <input type="hidden"  name="cena" value="450" id="cena4">
                              <input type="text" class="form-control"  id="promocja4" placeholder="wpisz kod promocyjny" name="kod" onBlur="sklep_kod(4)"></td>
                              <td><button type="submit" class="btn btn-default" " id="zaplata4">Zapłać</button></td>
                            </tr>
                          </form>






			                      </tbody>
			                    </table>
                        </div>
                      </div>
                    </div>';
                  }
                  echo '<div class="opcja" onclick="aktywacja_sklep()">';
                if($waznosc<=0)
                {
                  echo "usługa nieaktywna";
                }else if ($waznosc==1)
                {
                  echo "pozostał ostatni dzień usługi";
                }else
                {
                  echo "aktywny: ".$waznosc." dni";
                }
                echo ' <i class="icon-basket"></i></div>';
                ?>
              </div>
              <!--powiadomienia-->
              <div class="col-auto opcja">
                <!--
                //php
                //php
                sprawdzanie powiadomien
                //php
                //php
                -->
                <i class="icon-bell"></i>
              </div>
               <!--opcje-->
              <div class="col-auto opcja">
                <i class="icon-cog"></i>
              </div>
              <!--wylogowywanie-->
              <div class="col-auto opcja" onclick="window.location='wyloguj.php';">
                <i class="icon-off"></i>
              </div>
            </div>
        </div>
         <!--narzedzia-->
        <div class="col-lg order-lg-1 przybornik">
          <div class="row ">
             <!--sesje-->
            <div class="col-auto opcja">
              <i class="icon-camera"></i>
               sesje
            </div>
             <!--klienci-->
            <div class="col-auto opcja" onclick="on_klient()">
              <i class="icon-address-book"></i>
               klienci
            </div>
             <!--czat-->
            <div class="col-auto opcja">
              <!--
              //php
              //php
              sprawdzanie czatu
              //php
              //php
              -->
            <i class="icon-chat-empty"></i>
             czat
          </div>
          </div>
        </div>
      </div>
      <!--treść strony-->
    <div  id="treść">
      <div class="row justify-content-center">
        <div class="col-xl-5 " >
          <div class="row justify-content-between">
            <div class="col-sm-3   klient" style="padding-top:20px;" onclick="dodajsesje()">
              <i class="icon-plus" style="font-size:50px;color:green;"></i>
              <br/><span>Dodaj nowego klienta</span>
            </div>
            <div class="col-sm-3   klient">
              <img src="data/aa.jpg" class="img-fluid" alt="Sample photo">
              <br/><span>Jan Kowalski</span>
            </div>
            <div class="col-sm-3  klient" >
              <img src="data/aa.jpg" class="img-fluid" alt="Sample photo">
              <br/><span>Jan Kowalski</span>
            </div>
          </div>
        </div>
        <div style="width:2%;">
        </div>
        <div class="col-xl-5 ">
          <div class="row justify-content-between">
            <div class="col-sm-3 klient" >
              <img src="data/aa.jpg" class="img-fluid" alt="Sample photo">
              <br/><span>Jan Kowalski</span>
            </div>
            <div class="col-sm-3 klient">
              <img src="data/aa.jpg" class="img-fluid" alt="Sample photo">
              <br/><span>Jan Kowalski</span>
            </div>
            <div class="col-sm-3 klient" >
              <img src="data/aa.jpg" class="img-fluid" alt="Sample photo">
              <br/><span>Jan Kowalski</span>
            </div>
          </div>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-xl-5 " >
          <div class="row justify-content-between">
            <div class="col-sm-3   klient" style="padding-top:20px;">
              <i class="icon-plus" style="font-size:50px;color:green;"></i>
              <br/><span>Dodaj nowego klienta</span>
            </div>
            <div class="col-sm-3   klient">
              <img src="data/aa.jpg" class="img-fluid" alt="Sample photo">
              <br/><span>Jan Kowalski</span>
            </div>
            <div class="col-sm-3  klient" >
              <img src="data/aa.jpg" class="img-fluid" alt="Sample photo">
              <br/><span>Jan Kowalski</span>
            </div>
          </div>
        </div>
        <div style="width:2%;">
        </div>
        <div class="col-xl-5 ">
          <div class="row justify-content-between">
            <div class="col-sm-3 klient" >
              <img src="data/aa.jpg" class="img-fluid" alt="Sample photo">
              <br/><span>Jan Kowalski</span>
            </div>
            <div class="col-sm-3 klient">
              <img src="data/aa.jpg" class="img-fluid" alt="Sample photo">
              <br/><span>Jan Kowalski</span>
            </div>
            <div class="col-sm-3 klient" >
              <img src="data/aa.jpg" class="img-fluid" alt="Sample photo">
              <br/><span>Jan Kowalski</span>
            </div>
          </div>
        </div>
      </div>
    </div>

   



 
 
 
 
 
 
 
 
 
  <?php
  echo '
  <div  class="komunikat" id="komunikat_glowny" style="color:black;';
if(isset($_GET["komunikat"]))
{
 echo 'display:block;">
          <div class="row justify-content-between" >
            <div class=" col-11"style="text-align: center;">
              '.$_GET["komunikat"].'
            </div>
            <div class=" col-1">
              <button type="button"  class="zamykanie_przycisk" aria-label="Close">
              <span aria-hidden="true"  onclick="zmknij_komunikat()">&times;</span>
            </button>
          </div>';
}else
{
  echo 'display:none;">';
}
echo '</div>
';
?>
    <script src='js/panel_sesja.js'></script>
    <script src='js/panel_klient.js'></script>
    <script src='js/panel.js'></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>
</html>