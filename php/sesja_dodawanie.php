<?php

session_start();
echo '
<div class="row justify-content-center">
    <div class="col-7 "  id="dodawanie_klienta_kontener">
        <center>
            <h2>Dodawanie nowej sesji</h2> <br/>
            <button type="button" class="btn btn-primary"  onclick="sesja_dodawanie_klienta_przycisk()">Dodaj nowego klienta</button>
            &nbsp lub &nbsp
            <button type="button" class="btn btn-primary" onclick="sesja_wybieranie_klienta_przycisk()">Wybierz istniejącego klienta</button>
        </center>
        <div id="nowy_klient" style="display:none;">
        <form enctype="multipart/form-data"action="php/skrypty/dodawanie_klienta.php" method="post">
        <fieldset class="form-group">
            <h5>Imie *</h5>
            <div class="input-group">
                <input type="text" class="dodawanie_klienta_input"  placeholder="Imie" id="dodawanie_klienta_input_imie"  onBlur="klient_imie()">
            </div>
        </fieldset>
        <fieldset class="form-group">
            <h5>Nazwisko *</h5>
            <div class="input-group">
                <input type="text" class="dodawanie_klienta_input"  placeholder="Nazwisko" id="dodawanie_klienta_input_nazwisko"  onBlur="klient_nazwisko()">
            </div>
        </fieldset>
        <fieldset class="form-group">
            <h5>E-mail *</h5>
            <div class="input-group">
                <input type="text" class="dodawanie_klienta_input"  placeholder="E-mail" id="dodawanie_klienta_input_email"  onBlur="klient_email()">
            </div>
        </fieldset>
        <fieldset class="form-group">
            <h5>Telefon(opcjonalnie)</h5>
            <div class="input-group">
                <input type="text" class="dodawanie_klienta_input"  placeholder="Telefon(opcjonalnie)" id="dodawanie_klienta_input_tel"  onBlur="klient_telefon()">
            </div>
        </fieldset>
        <fieldset class="form-group">
            <h5>miniaturka(opcjonalnie)</h5>
            <div class="input-group">
                <label class="custom-file" id="customFile">
                    <input type="file" class="custom-file-input" id="klient_zdjecie_input" name="plik" aria-describedby="fileHelp" accept=".jpg, .jpeg" onchange="klient_zdjecie()">
                    <span class="custom-file-control form-control-file"  id="klient_zdjecie_input_nazwa" ></span>
                </label>
            </div>
        </fieldset>
        <fieldset class="form-group">
        <p id="dodawanie_klienta_info"></p>
        <input class="btn " type="button" value="Input" onclick="klient_dodawanie_sesja()">
        </fieldset>
        </form>
        </div>
        <div id="wybieranie_klienta" style="display:none;" onscroll="test()">
     
        ';
        $id=0;
        $baza = pg_connect("host=localhost dbname=postgres user=Pracownik password=5w1n4");
        $query = "SELECT id, imie, nazwisko FROM \"klienci\" WHERE ".$_SESSION['id']." = ANY (id_fotograf) ORDER BY id DESC LIMIT 10;";
        $result = pg_query($query) ;
        if(pg_num_rows($result)>0)
        {
        while ($row = pg_fetch_assoc($result)) {
            
            echo ' <div class="row my-2 sesja_klient">
            <div class="col-md-4">';
            $sciezka='C:\xampp\htdocs\strona\fotografowie/'.$_SESSION["id"].'/klienci/'.$row["id"].'.jpg';
            if (file_exists($sciezka))
            {
            echo' <img class="img-fluid m-4" src=" fotografowie/'.$_SESSION["id"].'/klienci/'.$row["id"].'.jpg" style="height:100px"> </div>';
            }else
            {
                echo' <img class="img-fluid m-4" src="data/aa.jpg" style="height:100px"> </div>';
            }echo'
           
            <div class="col-md-4">
            <h4 class="m-4">'.$row["imie"].' '.$row["nazwisko"].'</h4>
            </div>
            <div class="col-md-4">
              <a class="btn btn-primary  btn-lg m-4" onclick="wybor_klienta('.$row["id"].')">Dodaj sesje</a>
            </div>
          </div>';
          $id=$row["id"];

        }
        }else
        {
            echo'
            <center>
            <h2>Nie posiadasz jeszcze żadnego klienta</h2> <br/>
        </center>
            ';
        }
        echo'<input type="hidden" value="'.  $id.'"></div>
    </div>
</div>

';

?>