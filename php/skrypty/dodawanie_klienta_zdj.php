<?php
session_start();
if(!isset($_SESSION['id']))
	{
            setcookie("sesja", "false", time() + 3600);
            header("Location:../index.php");
            return;
    }
$max_rozmiar = 1024*1024*5;
if (is_uploaded_file($_FILES['plik']['tmp_name'])) 
{
    if ($_FILES['plik']['size'] > $max_rozmiar) 
    {
        echo 'dodano klienta ale podane zdjęcie jest za duże';
    } else 
    {
        $odczyt = pathinfo($_FILES['plik']['name']);
        move_uploaded_file($_FILES['plik']['tmp_name'],'../../fotografowie/'.$_SESSION['id'].'/klienci/'.$_SESSION['klient'].'.'.$odczyt['extension']);
        echo "<span style='color:green'>klient dodany poprawnie</span>";
    } 
} else 
{
   echo 'Błąd przy przesyłaniu danych!';
}
?>